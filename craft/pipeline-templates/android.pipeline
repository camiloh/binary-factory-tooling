// Sometimes we need to include additional parameters to Craft which are specific to the project
def craftProjectParams = ""

// Where will the craftmaster configuration be found (relative to the working directory that is)?
def craftmasterConfig = "ci-utilities/craft/qt5/CraftConfig.ini"

// Determine if we need to build a specific version of this project
if( craftTarget != '' ) {
	// If we have a specified version (Craft target) then we need to pass this along to Craft
	craftProjectParams = "--target ${craftTarget}"
}

// Determine the F-Droid repositry for the resulting APKs
def fdroidRepository = "nightly"
def fdroidExpiry = "60"
if( craftBuildType == "Release" ) {
	fdroidRepository = "stable-releases"
	fdroidExpiry = "365"
}

// Request a node to be allocated to us
node( "AndroidSDK" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we want to make sure Craft is ready to go
		stage('Preparing Craft') {
			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/binary-factory-tooling.git']]
			]

			// Make sure the Git checkouts are up to date
			sh """
				python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository https://invent.kde.org/packaging/craftmaster.git --into ~/Craft/BinaryFactory/
				python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository https://invent.kde.org/sysadmin/ci-utilities.git --into ~/Craft/BinaryFactory/
				python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository https://invent.kde.org/sysadmin/binary-factory-tooling.git --into ~/Craft/BinaryFactory/
			"""

			// Update Craft itself
			sh """
				cd ~/Craft/BinaryFactory/
				python3 craftmaster/CraftMaster.py --config "${craftmasterConfig}" --target ${craftPlatform} --config-override "binary-factory-tooling/craft/configs/binary-factory-override.ini" --setup
				python3 craftmaster/CraftMaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c -i craft
			"""
		}

		stage('Installing Dependencies') {
			// Ask Craftmaster to ensure all the dependencies are installed for this application we are going to be building
			sh """
				cd ~/Craft/BinaryFactory/
				python3 craftmaster/CraftMaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --install-deps ${craftBuildBlueprint}
			"""
		}

		stage('Building') {
			// Actually build the application now
			sh """
				cd ~/Craft/BinaryFactory/
				python3 craftmaster/CraftMaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} --no-cache ${craftProjectParams} ${craftRebuildBlueprint}
			"""
		}

		stage('Packaging') {
			// Now generate the APK for it
			sh """
				cd ~/Craft/BinaryFactory/
                python3 craftmaster/CraftMaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --package ${craftBuildBlueprint}
			"""

			// Now copy it to the Jenkins workspace so it can be grabbed from there
			sh """
				cd ~/Craft/BinaryFactory/
				packageDir=\$(python3 "craftmaster/CraftMaster.py" --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} -q --get "packageDestinationDir()" virtual/base)
				cp -vf \$packageDir/*.apk \$WORKSPACE/
				cp -vf \$packageDir/metadata/*.zip \$WORKSPACE/
			"""

			// Stash the APKs and Fastlane metadata for signing
			stash includes: '*.apk, *.zip', name: 'apks'
		}
	}
}
}

// With the APKs all built, we now need to transfer over to the signing machine as plain APKs won't be of much use to people
// We should also use this opportunity to make the APKs available in our F-Droid repository
node( "AndroidSigner" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// Time to sign!
		stage('Signing APKs') {
			// Make sure we have a clean environment first
			deleteDir()

			// Retrieve the APKs we captured earlier
			unstash 'apks'

			// Perform the signing process
			// By default we use the Keystore and associated details from ~/keys/android-signing-config
			// However if there is a package specific configuration, then we will use that instead
			// All APK files will be subject to zipalign prior to being signed in accordance with the Google documentation on signing of APKs
			sh """
				bash ~/binary-factory-tooling/android/perform-signing.sh ${craftBuildBlueprint}
			"""
		}

		// Capture the APKs to make them downloadable through Jenkins for those who prefer to sideload the APK files
		stage('Capturing Package') {
			// Then we ask Jenkins to capture the generated installers as an artifact
			archiveArtifacts artifacts: '*.apk, *.zip', onlyIfSuccessful: true
		}

		// Publish to Google Play (goes before F-Droid, as that consumes the source artifacts)
		stage('Google Play Publishing') {
			sh """
				if ! [ -z "${craftGooglePlayId}" ]; then
					. ~/binary-factory-tooling/android/setup-fastlane.sh
					python3 ~/binary-factory-tooling/android/google-play-sync.py --app-id ${craftGooglePlayId} --metadata fastlane-${craftGooglePlayId}.zip --apk *.apk
				fi
			"""
		}

		// Finally, publish them to our F-Droid repository
		stage('F-Droid Publishing') {
			// We'll move the files into the repository now, and then invoke the script designated to publish these artifacts
			sh """
				export PATH=$HOME/.local/bin/:$HOME/bin/:$PATH

				mv *.apk ~/${fdroidRepository}/repository/repo/
				mkdir -p ~/${fdroidRepository}/fastlane/
				mv *.zip ~/${fdroidRepository}/fastlane/ 2> /dev/null || true
				python3 ~/binary-factory-tooling/android/generaterepo.py --fdroid-repository ~/${fdroidRepository}/repository/ --fastlane ~/${fdroidRepository}/fastlane/ --expiry ${fdroidExpiry}
			"""
		}
	}
}
}
